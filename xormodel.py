import tensorflow as tf
import numpy as np

dataset = [
    ([0, 1], 1),
    ([1, 1], 0),
    ([1, 0], 1),
    ([0, 0], 0)
]

dataX = np.array([d[0] for d in dataset])
dataY = np.array([d[1] for d in dataset])

inputs = tf.keras.layers.Input(shape=(2, ))
d1 = tf.keras.layers.Dense(16, activation='relu')(inputs)
d2 = tf.keras.layers.Dense(16, activation='relu')(d1)
outputs = tf.keras.layers.Dense(1, activation='sigmoid')(d2)
model = tf.keras.Model(inputs=inputs, outputs=outputs)

model.summary()
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])

model.fit(x=dataX, y=dataY, epochs=100, verbose=1, batch_size=1)

model.save('model.h5')

builder = tf.saved_model.save(model, 'XorModel')