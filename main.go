package main

import (
	"fmt"
	"math"

	tf "github.com/tensorflow/tensorflow/tensorflow/go"
)

func main() {
	// replace myModel and myTag with the appropriate exported names in the chestrays-keras-binary-classification.ipynb
	model, err := tf.LoadSavedModel("XorModel", []string{"serve"}, nil)

	if err != nil {
		fmt.Printf("Error loading saved model: %s\n", err.Error())
		return
	}

	defer model.Session.Close()

	tensor, err := tf.NewTensor([][]float32{[]float32{0, 0}, []float32{0, 1}, []float32{1, 1}, []float32{1, 0}})
	if err != nil {
		fmt.Println(err)
		return
	}

	result, err := model.Session.Run(
		map[tf.Output]*tf.Tensor{
			model.Graph.Operation("serving_default_input_1").Output(0): tensor, // Replace this with your input layer name
		},
		[]tf.Output{
			model.Graph.Operation("StatefulPartitionedCall").Output(0), // Replace this with your output layer name
		},
		nil,
	)

	if err != nil {
		fmt.Printf("Error running the session with input, err: %s\n", err.Error())
		return
	}

	for i, res := range result[0].Value().([][]float32) {
		fmt.Printf("Input: %v, Result value: %d \n", (tensor.Value()).([][]float32)[i], int(math.Round(float64(res[0]))))
	}

}
